1. Apakah perbedaan antara JSON dan XML?
- JSON merupakan JavaScript Object Notation, sedangkan XML merupakan Extensible Markup Language.
- JSON diturunkan dari bahasa JavaScript, sedangkan XML berasal dari SGML (Standard Generalized Markup Language).
- JSON digunakan untuk merepresentasikan suatu objek, sedangkan XML digunakan sebagai bahasa markup yang menggunakan struktur tag untuk merepresentasikan suatu data.
- JSON tidak menggunakan tag akhir pada kodenya, sedangkan XML menggunakan tag akhir.
- JSON tidak mendukung penggunaan comment, sedangkan XML mendukung penggunaan comment.
- File JSON lebih mudah dibaca dibandingkan XML yang relatif sulit untuk ditafsirkan.

2. Apakah perbedaan antara HTML dan XML?
- HTML adalah markup language, sedangkan XML adalah standard markup language yang mendefinisikan markup language lainnya.
- HTML tidak case-sensitive, sedangkan XML case-sensitive.
- HTML berguna sebagai programming language dan presentation language, sedangkan XML tidak keduanya.
- HTML mempunyai tag yang sudah didefinisikan, sedangkan XML fleksibel dalam urusan tag sehingga tag didefinisikan ketika dibutuhkan.
- HTML tidak membutuhkan tag akhir, sedangkan XML membutuhkan tag akhir.
- HTML digunakan untuk menampilkan data, sedangkan XML digunakan untuk menyimpan dan mengirimkan data.

Referensi: 
- https://www.geeksforgeeks.org/difference-between-json-and-xml/
- https://www.upgrad.com/blog/html-vs-xml/