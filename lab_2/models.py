from django.db import models

# Create your models here.
class Note(models.Model):
    receiver = models.CharField(max_length=30)
    sender = models.CharField(max_length=30)
    title = models.CharField(max_length=30)
    message = models.TextField(primary_key=True)
