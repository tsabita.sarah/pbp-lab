import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

void main() => runApp(const MyApp());

/// This is the main application widget.
class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  static const String _title = 'Learning App';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      theme: ThemeData(
        // Define the default `TextTheme`. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: const TextTheme(
          headline6: TextStyle(fontSize: 25.0),
          bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        ),
      ),
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.toc),
          title: const Text(_title),
          centerTitle: true,
          backgroundColor: Colors.purple,
        ),
        body: const MyStatefulWidget(),
      ),
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  static TextEditingController controller1 = TextEditingController();
  static TextEditingController controller2 = TextEditingController();
  List<String> array1 = ["Google", "Facebook", "Twitter"];
  List<String> array2 = [
    "https://www.google.com",
    "https://www.facebook.com",
    "https://www.twitter.com"
  ];

  // @override
  // void dispose() {
  //   // Clean up the controller when the widget is disposed.
  //   controller1.dispose();
  //   controller2.dispose();
  //   super.dispose();
  // }

  clearTextInput() {
    controller1.clear();
    controller2.clear();
  }

  _getRequests() async {}

  getArray1() {
    return array1;
  }

  getArray2() {
    return array2;
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Text('Add Content',
              style: TextStyle(
                fontSize: 34,
                height: 2.0,
              )),
          Text('This field is required.',
              style: TextStyle(
                fontSize: 14,
                height: 2.0,
              )),
          Text('Title:',
              style: TextStyle(
                fontSize: 18,
                height: 2.0,
              )),
          TextFormField(
            controller: controller1,
            validator: (String value) {
              if (value == null || value.isEmpty) {
                return 'Fill out this field';
              }
              return null;
            },
          ),
          Text('This field is required.',
              style: TextStyle(
                fontSize: 14,
                height: 2.0,
              )),
          Text('File:',
              style: TextStyle(
                fontSize: 18,
                height: 2.0,
              )),
          TextFormField(
            controller: controller2,
            validator: (String value) {
              if (value == null || value.isEmpty) {
                return 'Fill out this field';
              }
              return null;
            },
          ),
          Padding(
            padding: const EdgeInsets.all(16),
            child: ElevatedButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Successfully added!')),
                  );
                  Navigator.of(context)
                      .push(
                        new MaterialPageRoute(
                            builder: (_) => new DestinationScreen()),
                      )
                      .then((val) => val ? _getRequests() : null);
                  // clearTextInput();
                }
              },
              child: const Text('SUBMIT'),
              style: ElevatedButton.styleFrom(primary: Colors.green),
            ),
          ),
        ],
      ),
    );
  }
}

class DestinationScreen extends StatefulWidget {
  DestinationScreen({Key key}) : super(key: key);

  @override
  _DestinationScreenState createState() => _DestinationScreenState();
}

class _DestinationScreenState extends State<DestinationScreen> {
  static const String _title = 'Learning App';
  final statefulwidg = _MyStatefulWidgetState();

  bool _isVisible = true;

  void showToast() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.toc),
        title: const Text(_title),
        centerTitle: true,
        backgroundColor: Colors.purple,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: Align(
              alignment: Alignment.centerLeft,
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pop(context, true);
                },
                child: const Text('+'),
                style: ElevatedButton.styleFrom(
                    primary: Colors.green, shape: CircleBorder()),
              ),
            ),
          ),
          for (var i = 0; i < 3; i++)
            Visibility(
              visible: _isVisible,
              child: Card(
                  shape: RoundedRectangleBorder(
                    side: BorderSide(
                      color: Colors.grey.withOpacity(0.2),
                      width: 1,
                    ),
                  ),
                  child: Container(
                      width: 400,
                      height: 50,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            new RichText(
                              text: new TextSpan(
                                children: [
                                  new TextSpan(
                                    text: statefulwidg.getArray1()[i],
                                    style: new TextStyle(color: Colors.blue),
                                    recognizer: new TapGestureRecognizer()
                                      ..onTap = () {
                                        launch(statefulwidg.getArray2()[i]);
                                      },
                                  ),
                                ],
                              ),
                            ),
                            ElevatedButton(
                              onPressed: showToast,
                              child: const Text('DELETE'),
                              style:
                                  ElevatedButton.styleFrom(primary: Colors.red),
                            )
                          ]))),
            ),
        ],
      ),
    );
  }
}
