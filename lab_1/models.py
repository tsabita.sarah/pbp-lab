from django.db import models
from datetime import datetime, date

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here

class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.CharField(max_length=30, primary_key=True)
    dob = models.DateField(default=date(2000, 1, 1))
    # TODO Implement missing attributes in Friend model
